package com.example.p03_listviewpersonalizado;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;


public class AlumnoAdapter extends BaseAdapter implements Filterable {

    /*int groupId;
    Activity Context;
    ArrayList<AlumnoItem> list;
    ArrayList<AlumnoItem> listaOriginal;*/
    LayoutInflater inflater;
    private Context newContex;
    private List<AlumnoItem> newData;
    private List<AlumnoItem> newFilteredData;

    /*public AlumnoAdapter(Activity Context, int groupId, int id, ArrayList<AlumnoItem> list) {
        super(Context,id,list);
        this.list = list;
        listaOriginal = new ArrayList<>();
        listaOriginal.addAll(list);
        inflater = (LayoutInflater)Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;
    }*/

    public AlumnoAdapter(Context context, List<AlumnoItem> data) {
        newContex = context;
        newData = data;
        newFilteredData = new ArrayList<>(data);
    }

    @Override
    public int getCount() {
        return newFilteredData.size();
    }

    @Override
    public Object getItem(int posicion) {
        return newFilteredData.get(posicion);
    }

    @Override
    public long getItemId(int posicion) {
        return posicion;
    }

    @Override
    public View getView(int posicion, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(newContex);
            convertView = inflater.inflate(R.layout.alumnoitem_layout, parent, false);

            holder = new ViewHolder();
            holder.imgFoto = convertView.findViewById(R.id.imgAlumno);
            holder.lblMatricula = convertView.findViewById(R.id.lblNombreAlum);
            holder.lblNombre = convertView.findViewById(R.id.lblMatricula);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        AlumnoItem item = newFilteredData.get(posicion);

        holder.imgFoto.setImageResource(item.getImageId());
        holder.lblMatricula.setText(item.getTextNombreAlum());
        holder.lblNombre.setText(item.getTextMatricula());
        return convertView;
    }

    public View getDropDownView(int posicion, View convertView,ViewGroup parent) {
        return getView(posicion,convertView,parent);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<AlumnoItem> filteredList = new ArrayList<>();

                if (constraint == null || constraint.length() == 0) {
                    filteredList.addAll(newData);
                } else {
                    String filterPattern = constraint.toString().toLowerCase().trim();
                    for (AlumnoItem item : newData) {
                        if (item.getTextMatricula().toLowerCase().contains(filterPattern) || item.getTextNombreAlum().toLowerCase().contains(filterPattern)) {
                            filteredList.add(item);
                        }
                    }
                }

                results.values = filteredList;
                results.count = filteredList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                newFilteredData.clear();
                newFilteredData.addAll((List<AlumnoItem>) results.values);
                notifyDataSetChanged();
            }
        };
    }

    static class ViewHolder {
        ImageView imgFoto;
        TextView lblMatricula;
        TextView lblNombre;
    }


    /*public View getView(int posicion, View convertView, ViewGroup parent) {
        View itemView = inflater.inflate(groupId, parent, false);
        ImageView imagen = (ImageView) itemView.findViewById(R.id.imgAlumno);
        imagen.setImageResource(list.get(posicion).getImageId());
        TextView textMatricula = (TextView) itemView.findViewById(R.id.lblMatricula);
        textMatricula.setText(list.get(posicion).getTextMatricula());
        TextView textNombreAlum = (TextView) itemView.findViewById(R.id.lblNombreAlum);
        textNombreAlum.setText(list.get(posicion).getTextNombreAlum());
        return itemView;
    }

    public View getDropDownView(int posicion, View convertView, ViewGroup parent) {
        return getView(posicion, convertView, parent);
    }

    public void filtrado(final String txtBuscar) {
        int longitud = txtBuscar.length();
        if(longitud == 0) {
            list.clear();
            list.addAll(listaOriginal);
        } else {
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                List<AlumnoItem> collection = list.stream().filter(i -> i.getTextMatricula().toLowerCase().contains(txtBuscar.toLowerCase())).collect(Collectors.toList());
                list.clear();
                list.addAll(collection);
            } else {
                for (AlumnoItem ai: listaOriginal) {
                    if (ai.getTextMatricula().toLowerCase().contains(txtBuscar.toLowerCase())) {
                        list.add(ai);
                    }
                }
            }
        }
    }*/
}
