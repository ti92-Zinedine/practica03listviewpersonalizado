package com.example.p03_listviewpersonalizado;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ListView lv;
    private SearchView searchView;
    private AlumnoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ArrayList<AlumnoItem> list = new ArrayList<>();
        list.add(new AlumnoItem(getString(R.string.item2019030344), getString(R.string.msg2019030344), R.drawable.a2019030344, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030174), getString(R.string.msg2020030174), R.drawable.a2020030174, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030176), getString(R.string.msg2020030176), R.drawable.a2020030176, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030181), getString(R.string.msg2020030181), R.drawable.a2020030181, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030184), getString(R.string.msg2020030184), R.drawable.a2020030184, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030189), getString(R.string.msg2020030189), R.drawable.a2020030189, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030199), getString(R.string.msg2020030199), R.drawable.a2020030199, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030212), getString(R.string.msg2020030212), R.drawable.a2020030212, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030241), getString(R.string.msg2020030241), R.drawable.a2020030241, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030243), getString(R.string.msg2020030243), R.drawable.a2020030243, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030249), getString(R.string.msg2020030249), R.drawable.a2020030249, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030264), getString(R.string.msg2020030264), R.drawable.a2020030264, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030268), getString(R.string.msg2020030268), R.drawable.a2020030268, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030292), getString(R.string.msg2020030292), R.drawable.a2020030292, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030304), getString(R.string.msg2020030304), R.drawable.a2020030304, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030306), getString(R.string.msg2020030306), R.drawable.a2020030306, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030313), getString(R.string.msg2020030313), R.drawable.a2020030313, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030315), getString(R.string.msg2020030315), R.drawable.a2020030315, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030322), getString(R.string.msg2020030322), R.drawable.a2020030322, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030325), getString(R.string.msg2020030325), R.drawable.a2020030325, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030327), getString(R.string.msg2020030327), R.drawable.a2020030327, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030329), getString(R.string.msg2020030329), R.drawable.a2020030329, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030332), getString(R.string.msg2020030332), R.drawable.a2020030332, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030333), getString(R.string.msg2020030333), R.drawable.a2020030333, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030389), getString(R.string.msg2020030389), R.drawable.a2020030389, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030766), getString(R.string.msg2020030766), R.drawable.a2020030766, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030771), getString(R.string.msg2020030771), R.drawable.a2020030771, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030777), getString(R.string.msg2020030777), R.drawable.a2020030777, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030799), getString(R.string.msg2020030799), R.drawable.a2020030799, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030808), getString(R.string.msg2020030808), R.drawable.a2020030808, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030819), getString(R.string.msg2020030819), R.drawable.a2020030819, getString(R.string.carreraTec)));
        list.add(new AlumnoItem(getString(R.string.item2020030865), getString(R.string.msg2020030865), R.drawable.a2020030865, getString(R.string.carreraTec)));

        adapter = new AlumnoAdapter(this, list);

        lv = findViewById(R.id.listview1);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(adapterView.getContext(),getString(R.string.msgSeleccionado).toString() +" "+((AlumnoItem) adapterView.getItemAtPosition(i)).getTextMatricula(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview, menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint("Escriba aquí para buscar");

        setupSearchView();
        return true;
    }


    public void setupSearchView() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if(adapter != null) {
                    adapter.getFilter().filter(s);
                }
                return true;
            }
        });
    }
}