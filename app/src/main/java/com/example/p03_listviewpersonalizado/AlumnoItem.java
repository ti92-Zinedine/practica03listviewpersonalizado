package com.example.p03_listviewpersonalizado;

public class AlumnoItem {
    private int id;
    private String carrera;
    private String textMatricula;
    private String textNombreAlum;
    private Integer imageId;

    public AlumnoItem(){}

    public AlumnoItem(String text, String text2, Integer imageId, String carrera) {
        this.textMatricula = text2;
        this.textNombreAlum = text;
        this.imageId = imageId;
        this.carrera = carrera;
    }


    // Métodos Set y Get
    public int getTextId() { return id; }
    public void setTextId(int id) {
        this.id = id;
    }
    public String getTextCarrera() { return carrera; }
    public void setTextCarrera(String carrera) { this.carrera = carrera; }
    public String getTextMatricula() {return textMatricula; }
    public void setTextMatricula(String text2) {this.textMatricula = text2; }
    public String getTextNombreAlum() {return textNombreAlum; }
    public void setTextNombreAlum(String text) {this.textNombreAlum = text; }
    public Integer getImageId() {return imageId; }
    public void setImageId(Integer imageId) {this.imageId = imageId; }
}
