package Modelo;

import com.example.p03_listviewpersonalizado.AlumnoItem;

public interface Persistencia {

    public void openDataBase();
    public void closeDataBase();
    public long insertAlumno(AlumnoItem alumno);
    public long updateAlumno(AlumnoItem alumno);
    public void deleteAlumnos(int id);

}
